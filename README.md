# Flectra Community / brand

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[contract_brand](contract_brand/) | 2.0.1.0.1|         This module allows you to manage branded contracts.        It adds a brand field on the contract and propagate the value on the        invoices.
[product_brand](product_brand/) | 2.0.1.1.2| Product Brand Manager
[product_brand_tag](product_brand_tag/) | 2.0.1.1.1| Add tags to product brand
[sale_brand](sale_brand/) | 2.0.1.0.1| Send branded sales orders
[brand_external_report_layout](brand_external_report_layout/) | 2.0.1.0.2|         This module allows you to have a different layout by brand for your        external reports.
[product_brand_tag_secondary](product_brand_tag_secondary/) | 2.0.1.0.0| Add secondary tags to product brand for a second level of categorization.
[analytic_brand](analytic_brand/) | 2.0.1.0.0|         This addon associate an analytic account to a brand that will be used        as a default value where the brand is used if the analytic accounting        is activated
[product_brand_purchase](product_brand_purchase/) | 2.0.1.0.0|         This module allows to work with product_brand in purchase reports.
[product_brand_multicompany](product_brand_multicompany/) | 2.0.1.0.0| Define rules product brand's visibility by company
[product_brand_social_responsibility](product_brand_social_responsibility/) | 2.0.1.1.0| Provide CSR info on brands.
[brand](brand/) | 2.0.1.0.2|         This is a base addon for brand modules. It adds the brand object and        its menu and define an abstract model to be inherited from branded        objects
[account_brand](account_brand/) | 2.0.1.0.1| Send branded invoices and refunds


